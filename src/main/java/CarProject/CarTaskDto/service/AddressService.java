package CarProject.CarTaskDto.service;

import CarProject.CarTaskDto.dto.AddressRequest;
import CarProject.CarTaskDto.dto.AddressResponse;
import CarProject.CarTaskDto.model.Address;
import CarProject.CarTaskDto.model.Branch;
import CarProject.CarTaskDto.repository.AddressRepository;
import CarProject.CarTaskDto.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;

    public AddressResponse create(Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);
    }

    public AddressResponse update(Long branchId, Long addressId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address =addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));
        address.setName(request.getName());
     branchRepository.save(branch);
     return modelMapper.map(address,AddressResponse.class);

    }

    public void delete(Long branchId,Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address=addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));
       branch.setAddress(null);
        addressRepository.deleteById(addressId);

    }

    public AddressResponse get(Long branchId, Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address=addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));

        return modelMapper.map(address,AddressResponse.class);

    }
}

