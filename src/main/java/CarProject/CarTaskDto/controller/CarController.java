package CarProject.CarTaskDto.controller;

import CarProject.CarTaskDto.dto.CarDto;
import CarProject.CarTaskDto.dto.CreateCarDto;
import CarProject.CarTaskDto.dto.UpdateCarDto;
import CarProject.CarTaskDto.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    private final CarService carService;
    public CarController(CarService carService){

        this.carService=carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto dto){

        carService.create(dto);
    }
    @PutMapping
    public void update(@RequestBody UpdateCarDto dto){
        carService.update(dto);

    }
    @GetMapping("{id}")
    public CarDto get(@PathVariable Integer id){

        return carService.get(id);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id){

        carService.delete(id);
    }
    @GetMapping
    public List<CarDto> getAll(){
        return carService.getAll();
    }

}
