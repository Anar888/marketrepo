package CarProject.CarTaskDto.dto;

import CarProject.CarTaskDto.enums.RegionType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegionRequest {
    @Enumerated(EnumType.STRING)
    RegionType name;
}
