package CarProject.CarTaskDto;

import CarProject.CarTaskDto.model.Address;
import CarProject.CarTaskDto.model.Branch;
import CarProject.CarTaskDto.model.Market;
import CarProject.CarTaskDto.repository.AddressRepository;
import CarProject.CarTaskDto.repository.BranchRepository;
import CarProject.CarTaskDto.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class CarTaskDtoApplication implements CommandLineRunner {
	private final MarketRepository marketRepository;
	private final BranchRepository branchRepository;
	private final AddressRepository adressRepository;

	public static void main(String[] args) {
		SpringApplication.run(CarTaskDtoApplication.class, args);
	}




	@Override
	@Transactional
	public void run(String... args) throws Exception {








//
//
//		Market market2 = Market.builder()
//                .name("Bravo")
//                .type("SuperMarket")
//                .build();
//		marketRepository.save(market2);



//
//
//		Market market = Market.builder()
//                .name("Grand")
//                .type("SuperMarket")
//                .build();
//
//        Branch korogluBranch = Branch.builder()
//                .name("Ecemi")
//                .countOfEmployee(222)
//                .market(market)
//                .build();
//
//        Branch ahmadliBranch = Branch.builder()
//                .name("Gunesli")
//                .countOfEmployee(11)
//                .market(market)
//                .build();
//
//        market.getBranches().add(korogluBranch);
//        market.getBranches().add(ahmadliBranch);
//
//
//        Address address1 = Address.builder()
//                .name("Ecemi pr, 20")
//                .build();
//
//        Address address2 = Address.builder()
//                .name("gunesli mall")
//                .build();
//
//        korogluBranch.setAddress(address1);
//        ahmadliBranch.setAddress(address2);
//
//        marketRepository.save(market);


	}

	}

