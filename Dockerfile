FROM openjdk:17-alpine
COPY build/libs/CarTaskDto-0.0.1-SNAPSHOT.jar /car/
CMD ["java","-jar","/car/CarTaskDto-0.0.1-SNAPSHOT.jar"]