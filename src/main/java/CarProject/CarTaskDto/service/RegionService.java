package CarProject.CarTaskDto.service;

import CarProject.CarTaskDto.dto.BranchRequest;
import CarProject.CarTaskDto.dto.BranchResponse;
import CarProject.CarTaskDto.dto.RegionRequest;
import CarProject.CarTaskDto.dto.RegionResponse;
import CarProject.CarTaskDto.model.Branch;
import CarProject.CarTaskDto.model.Market;
import CarProject.CarTaskDto.model.Region;
import CarProject.CarTaskDto.repository.MarketRepository;
import CarProject.CarTaskDto.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    private final MarketRepository marketRepository;

    private final ModelMapper modelMapper;

    public RegionResponse create(Long marketId, RegionRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Market with id %s not found", marketId)));
        Region region = modelMapper.map(request, Region.class);
        market.getRegions().add(region);
        regionRepository.save(region);
        return modelMapper.map(region, RegionResponse.class);
    }


    public RegionResponse update(Long marketId, Long regionId, RegionRequest request) {
        Region region=regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Region with id %s not found", regionId)));

        region.setName(request.getName());
        regionRepository.save(region);
        return modelMapper.map(region,RegionResponse.class);


    }

    public RegionResponse get(Long regionId) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(
                        String.format("Region with id %s not found", regionId)));

        return modelMapper.map(region,RegionResponse.class);
    }

    public void delete(Long marketId,Long regionId) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(
                        String.format("Region with id %s not found", regionId)));
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Market with id %s not found", marketId)));
        market.getRegions().remove(region);
        marketRepository.save(market);

    }
}
