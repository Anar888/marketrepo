package CarProject.CarTaskDto.controller;

import CarProject.CarTaskDto.dto.*;
import CarProject.CarTaskDto.service.RegionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final RegionService regionService;
    @PostMapping("market/{marketId}")
    public RegionResponse create(@PathVariable Long marketId, @RequestBody RegionRequest request) {
        return regionService.create(marketId, request);
    }
    @PutMapping("market/{marketId}/reg/{regionId}")
    public RegionResponse update(@PathVariable Long marketId,
                                 @PathVariable Long regionId,
                                 @RequestBody RegionRequest request) {
        return regionService.update(marketId, regionId, request);
    }
    @GetMapping("/regions/{regionId}")
    public RegionResponse get(@PathVariable Long regionId) {
        return regionService.get(regionId);
    }
    @DeleteMapping("market/{marketId}/reg/{regionId}")
    public void delete( @PathVariable Long marketId,@PathVariable Long regionId ) {
        regionService.delete(marketId, regionId);
    }
}
