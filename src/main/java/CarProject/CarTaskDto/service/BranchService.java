package CarProject.CarTaskDto.service;

import CarProject.CarTaskDto.dto.BranchRequest;
import CarProject.CarTaskDto.dto.BranchResponse;
import CarProject.CarTaskDto.dto.MarketResponse;
import CarProject.CarTaskDto.model.Branch;
import CarProject.CarTaskDto.model.Market;
import CarProject.CarTaskDto.repository.BranchRepository;
import CarProject.CarTaskDto.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;


    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Market with id %s not found", marketId)));
        Branch branch=branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));

        branch.setName(request.getName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        marketRepository.save(market);
        return modelMapper.map(branch,BranchResponse.class);


    }

    public List<BranchResponse> getAll() {
        return branchRepository
                .findAll()
                .stream()
                .map(branch -> modelMapper.map(branch, BranchResponse.class))
                .collect(Collectors.toList());
    }

    public BranchResponse get(Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        return modelMapper.map(branch,BranchResponse.class);
    }

    public void delete(Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        branchRepository.deleteById(branchId);


    }
}
