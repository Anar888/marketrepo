package CarProject.CarTaskDto.repository;

import CarProject.CarTaskDto.model.Address;
import CarProject.CarTaskDto.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {

}
